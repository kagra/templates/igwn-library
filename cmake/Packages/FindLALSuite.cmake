# Include custom standard package
list(PREPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR}/../Modules)
include(FindPackageStandard)

# Load using standard package finder
find_package_standard(
  LALSuite
  NAMES lal lalinspiral lalmetaio lalburst lalpulsar lalframe lalsimulation lalinference lalsupport
  HEADERS "lal"
  PATHS $ENV{LALSUITE}
)
