.PHONY: $(filter-out .%, $(MAKECMDGOALS))

# Setup environment variables
ifneq (,$(wildcard .env))
    include .env
    export
endif

export PLATFORM  ?= linux/amd64
export CONTAINER ?= $(shell basename $(PWD) | tr ' ' '-')
ifneq (,$(NOCACHE))
    export NOCACHE = "--no-cache"
endif

ifndef ENVNAME
$(error ENVNAME is not defined. Please set it in .env or in your environment context)
endif

export MODULE    ?= $(shell echo $(PROJECT_NAME) | tr ' ' '-' | tr '[:upper:]' '[:lower:]' | tr '-' '_')
export LINT      ?= modules tests
export DISCOVER  ?= "discover"

## Display the help message
help: usage
usage: motd
	@printf "${COLOR_TITLE_BLOCK}Project Application \"$(PROJECT_NAME)\"${COLOR_RESET}\n"
	@printf "\n"
	@printf "${COLOR_COMMENT}Usage:${COLOR_RESET}\n"
	@printf "\tmake [target]\n\n"
	@printf "${COLOR_COMMENT}Available targets:${COLOR_RESET}\n"
	@awk 'BEGIN { inComment = 0 } \
	/^[a-zA-Z\-\_0-9\@]+:/ { \
		if (inComment) { \
			helpCommand = substr($$1, 0, index($$1, ":")); \
			printf "\t${COLOR_INFO}%-16s${COLOR_RESET} %s\n", helpCommand, helpMessage; \
			inComment = 0; \
		} \
	} \
	/^##/ { \
		helpMessage = substr($$0, 3); \
		inComment = 1; \
	} \
	' $(MAKEFILE_LIST)

## Complete procedure to build and install
all: motd env build install
motd:
ifneq ($(MOTD),0)
	@if [ -s "share/motd" ] && [ ! -z "$$(which envsubst)" ]; then echo "$$(env | envsubst < share/motd)"; fi
endif

## Create a Conda environment from environment.yml
env: motd
	@printf "Checking for environments.. "
	@if [ -f "environment.yml" ]; then \
		echo "DONE"; \
		$(MAKE) --no-print-directory .virtualenv; \
	else \
		echo "no \`environment.yml\` file found. Nothing to do."; \
	fi

.virtualenv: environment.yml requirements.txt
	@$(MAKE) .envcheck

	@if mamba env list | grep -q $(ENVNAME); then \
		echo "Updating existing environment \`$(ENVNAME)\`"; \
		mamba env update -n $(ENVNAME) -f environment.yml; \
	else \
		echo "Creating new environment \`$(ENVNAME)\`"; \
		mamba env create -n $(ENVNAME) -f environment.yml; \
	fi

# @TODO: To be removed in the future
	@ENVPATH=$$(conda info --envs | grep -E "^$(ENVNAME)" | awk '{print $$NF}'); \
	ln -sf $$ENVPATH/lib/libgsl.dylib $$ENVPATH/lib/libgsl.25.dylib

# @TODO: Poor solution for fixing multiprocessing issue on macOS
	@ENVPATH=$$(conda info --envs | grep -E "^$(ENVNAME)" | awk '{print $$NF}'); \
	[ "$$(uname)" = "Darwin" ] && sed -i '' "/set_start_method/d" $$ENVPATH/lib/python3.*/site-packages/pycbc/__init__.py || true

	@touch .virtualenv 2> /dev/null || true

.envcheck:
	@. $$(conda info --base)/etc/profile.d/conda.sh

	@command -v conda >/dev/null 2>&1 || { echo "Error: conda is not installed. Please install conda to use this command."; exit 1; }
	@command -v mamba >/dev/null 2>&1 || { conda activate base && conda install mamba; }
	@command -v mamba >/dev/null 2>&1 || { echo "Error: mamba is not installed. Please install mamba to use this command."; exit 1; }

## Building with debug symbols
debug: motd
	@MOTD=0 BUILDTYPE=Debug $(MAKE) -s configure

## Building in release mode
release: motd
	@MOTD=0 BUILDTYPE=Release $(MAKE) -s configure

## Configure the build environment and build
configure: motd
	@MOTD=0 ./bootstrap.sh $(BUILDTYPE)

## Building the project
build: motd
	@if [ ! -d "var/build" ]; then MOTD=0 $(MAKE) -s configure; fi
	@$(MAKE) -sC var/build lib

## Vendor dependencies
vendor: motd
	@$(MAKE) -sC vendor

## Compile the tools
tools: motd
	@if [ ! -d "var/build" ]; then BUILDTYPE=Release MOTD=0 $(MAKE) -s configure; fi
	@$(MAKE) -sC var/build tools

## Compile the examples
examples: motd
	@if [ ! -d "var/build" ]; then BUILDTYPE=Release MOTD=0 $(MAKE) -s configure; fi
	@$(MAKE) -sC var/build examples

## Test the project
tests: motd
	@python -m unittest $(DISCOVER)
	@if [ ! -d "var/build" ]; then BUILDTYPE=Release MOTD=0 $(MAKE) -s configure; fi
	@$(MAKE) -sC var/build tests

## Install the project in the final directory
install: motd
	@if [ ! -d "var/build" ]; then BUILDTYPE=Release MOTD=0 $(MAKE) -s configure; fi
	@$(MAKE) -sC var/build install

## Build the Docker image
image: motd
	@docker buildx build --platform=$(PLATFORM) -t $$(basename $(PWD)) $(NOCACHE) .

## Prepare the container (including warm up)
container:
	@$(MAKE) stop
	@docker compose -f docker-compose.yml up --build

## Start the container
start: motd
	@docker compose up -d

## Stop the container
stop: motd
	@docker compose down

## Restart the container
restart: motd
	@MOTD=0 $(MAKE) stop
	@MOTD=0 $(MAKE) start

## Run in a shell inside the container
shell: motd
	@MOTD=0 $(MAKE) start
	@CONTAINER_ID=$$(docker ps -aqf ancestor=$${CONTAINER}-container) && \
	 if [ ! -z "$$CONTAINER_ID" ]; then docker exec -it $$CONTAINER_ID /bin/bash; else echo "No running container found, please check \`docker ps\`"; fi

## Build project documentation and open in browser
pages: motd
	@if [ ! -d "var/build/share/doxygen" ]; then BUILDTYPE=Doc MOTD=0 $(MAKE) -s configure; fi
	@$(MAKE) -sC var/build pages
	@if [ -f "public/latest/index.html" ]; then open public/latest/index.html; fi

## Build project documentation
docs: motd
	@if [ ! -d "var/build/share/doxygen" ]; then BUILDTYPE=Doc MOTD=0 $(MAKE) -s configure; fi
	@$(MAKE) -sC var/build doxygen

## Build the python package and distribute it
modules: motd
	@mkdir -p var/install/lib var/build
	@ln -sfnT ../modules tools/$(MODULE)
	@ln -sfnT ../modules tests/$(MODULE)
	@ln -sfnT ../../modules var/build/modules
	@ln -sf   ../../pyproject.toml var/build
	@export EGG_CACHE=var/build/.eggs; \
	SITEPACKAGE="var/install/lib/python-$$(python --version | awk '{print $$2}' | cut -d '.' -f 1,2)/site-packages"; \
	 cd var/build && python -m build --skip-dependency-check --no-isolation --outdir ../../$$SITEPACKAGE ; \
	 [ $$? -eq 0 ] && echo "Distributed package is now available in \`./$$SITEPACKAGE\`" || exit 1

## Install the python package in the environment
pip: motd
	@SITEPACKAGE="var/install/lib/python-$$(python --version | awk '{print $$2}' | cut -d '.' -f 1,2)/site-packages"; \
	 mkdir -p $$SITEPACKAGE && pip install $$SITEPACKAGE/$(MODULE)-$(PROJECT_VERSION).tar.gz;

# @TODO: Please implement for publishing
## Publish the python package to Conda and PyPI
#publish: motd conda pypi
#conda: motd
#       @SITEPACKAGE="lib/python-$$(python --version | awk '{print $$2}' | cut -d '.' -f 1,2)/site-packages";\
# [...]
## Publish the python package to PyPI
#pypi: motd
#       @SITEPACKAGE="lib/python-$$(python --version | awk '{print $$2}' | cut -d '.' -f 1,2)/site-packages";\
#       python -m twine upload --repository pypitest $$SITEPACKAGE/$(MODULE)-$(PROJECT_VERSION).tar.gz
# [...]

## Run the linter for python package
lint: motd
	@flake8 $(LINT)

## Format the code using black
format: motd 
	@read -p "Make sure to commit your code first; Do you want to modify your code using black? (yes/no): " response; \
	if [ "$$response" = "yes" ]; then \
		black $(LINT)
	else \
		echo "Exiting..."; \
		exit 1; \
	fi

## Clean up build artifacts
clean: motd
	@$(RM) -rf var/cache/* var/log/* var/build/* 2> /dev/null || true

## Clean up build artifacts and remove the environment
distclean: clean
	@mamba env remove -n $(ENVNAME)
	@$(RM) -rf .virtualenv var/install var/build public build 2> /dev/null || true

%:
	@:
