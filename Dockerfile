FROM hepdock/root:6.34.02-debian12.8

ENV LANG=C.UTF-8
ENV DEBIAN_FRONTEND=noninteractive

# APT Package Manager
COPY Dockerfile.packages packages
RUN apt-get update -qq \
    && ln -sf /usr/share/zoneinfo/UTC /etc/localtime \
    && apt-get -y install $(awk -F '#' '{print $1}' packages) \
    && apt-get autoremove -y && apt-get clean -y \
    && rm -rf /var/cache/apt/archives/* && rm -rf /var/lib/apt/lists/*

# Conda Environment Manager
ENV PATH=/opt/conda/bin:$PATH
COPY requirements.txt /tmp/requirements.txt
RUN SHFILE="https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-$(uname)-$(uname -m).sh" \
    && wget -O conda.sh $SHFILE && sh ./conda.sh -b -p "/opt/conda" && rm conda.sh

# LSCSoft Packages
RUN git clone https://git.ligo.org/virgo/virgoapp/Fr.git /opt/framel \
    && mkdir -p /opt/framel/build && cd /opt/framel/build \
    && cmake .. && make -j$(($(nproc)-2)) && make install && rm -rf /opt/framel

RUN git clone https://git.ligo.org/lscsoft/metaio.git /opt/metaio \
    && cd /opt/metaio && git checkout release-8.5.0-v1 \
    && ./00boot && ./configure && make -j$(($(nproc)-2)) && make install && rm -rf /opt/metaio
 
RUN PYTHONPATH=$(python -m site --user-site) \
    && git clone --recursive https://git.ligo.org/lscsoft/lalsuite.git /opt/lalsuite \
    && pip install six numpy swig && cd /opt/lalsuite && ./00boot && ./configure \
    && make -j$(($(nproc)-2)) CFLAGS="-Wno-error=unused-function" && make install && rm -rf /opt/lalsuite

WORKDIR /root
COPY .condarc .
COPY .bashrc .
COPY .env .

CMD ["/bin/bash"]
