import logging
import sys, os, re

from logging.handlers import QueueHandler, QueueListener
from queue import Queue

class StreamHandler(logging.StreamHandler):
    def clearline(self, length=32):
        self.stream.write("\r")
        self.stream.write(' ' * length)
        self.stream.write("\r")
        self.stream.flush()

class LoggerFormatter(logging.Formatter):

    grey = "\x1b[38;20m"
    green = "\x1b[32;20m"
    yellow = "\x1b[33;20m"
    cyan = "\x1b[36;20m"
    purple = "\x1b[35;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    gray = "\x1b[90;20m"
    reset = "\x1b[0m"

    FORMATS = {
        logging.DEBUG   : f"{gray}%(asctime)s %(name)s{reset}\t   {cyan}%(levelname)s{reset} %(message)s",
        logging.INFO    : f"{gray}%(asctime)s %(name)s{reset}\t    {green}%(levelname)s{reset} %(message)s",
        logging.WARNING : f"{gray}%(asctime)s %(name)s{reset}\t {yellow}%(levelname)s{reset} %(message)s",
        logging.ERROR   : f"{gray}%(asctime)s %(name)s{reset}\t   {red}%(levelname)s{reset} %(message)s\t(%(filename)s:%(lineno)d)",
        logging.CRITICAL: f"{gray}%(asctime)s %(name)s{reset}\t{bold_red}%(levelname)s{reset} %(message)s\t(%(filename)s:%(lineno)d)"
    }

    def spacer(self, text):
        """Replace all visible characters with spaces but keep shell colors and tabs."""
        ansi_escape = re.compile(r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])')
        ansi_sequences = {match.start(): match.group() for match in ansi_escape.finditer(text)}
        replaced_text = re.sub(r'[^\t\x1B]', " ", text)
        for pos, ansi_seq in ansi_sequences.items():
            replaced_text = replaced_text[:pos] + ansi_seq + replaced_text[pos + len(ansi_seq):]

        return replaced_text

    def format(self, record):

        formatter = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(formatter)

        empty = logging.LogRecord(**record.__dict__, level=logging.DEBUG)
        empty.msg = ""

        spacer = self.spacer(formatter.format(empty))        
        message = record.msg.split("\n")
        if len(message) > 1:
            message = [message[0]] + [spacer + line for line in message[1:]]
        
        record.msg = "\n".join(message)
        return formatter.format(record)

class LoggerNameFilter(logging.Filter):
    def __init__(self, new_name):
        super().__init__()
        self.new_name = new_name

    def filter(self, record):
        record.name = self.new_name
        return True
    
class Logger:  # do not inherit from logging.Logger (cannot be pickled)

    name = f"{os.getpid()}"
    name_filter = None
    verbose = None
    quiet = None

    def __init__(self, name, **kwargs):

        self.verbose = kwargs.get("verbose", None)
        if self.verbose is None: self.verbose = os.getenv("VERBOSE", os.getenv("DEBUG", None))

        self.quiet = kwargs.get("quiet", None)
        if self.quiet is None: self.quiet = os.getenv("QUIET", None)

        self.size = kwargs.get("size", None)

        self.logger = logging.getLogger(' - '.join([self.name, name]))
        self.set_name(name)
        
        verbose = self.verbose if self.verbose is not None else Logger.verbose
        self.logger.setLevel(logging.DEBUG if verbose else logging.INFO)
        self.queue = Queue()
        
        # Clear existing handlers to avoid duplicates
        self.queue_listener = None
        self.close_handlers()

        # Reconfiguring stream handler
        if not self.quiet:
            out = StreamHandler(sys.stdout)
            err = StreamHandler(sys.stderr)
            self.add_handler(out, err)

        # Configure custom handlers
        out = kwargs.get("out", None)
        if not isinstance(out, logging.FileHandler):
            if isinstance(out, str): os.makedirs(os.path.dirname(out), exist_ok=True)
            if self.size is not None:
                out = logging.RotatingFileHandler(out) if isinstance(out, str) else None
            else:
                out = logging.FileHandler(out) if isinstance(out, str) else None
       
        err = kwargs.get("err", out)
        if not isinstance(err, logging.FileHandler):
            if isinstance(err, str): os.makedirs(os.path.dirname(err), exist_ok=True)
            if self.size is not None:
                err = logging.RotatingFileHandler(err) if isinstance(err, str) else None
            else:
                err = logging.FileHandler(err) if isinstance(err, str) else None

        self.add_handler(out, err)

    def close_handlers(self):

        self._stop_queue_listener()

        for handler in self.logger.handlers[:]:
            self.logger.removeHandler(handler)
            handler.close()

    def set_name(self, name):

        if self.name_filter:
            self.logger.removeFilter(self.name_filter)

        self.name_filter = LoggerNameFilter(' - '.join([self.name, name]))
        self.logger.addFilter(self.name_filter)

    def add_handler(self, out, err=None):

        self._stop_queue_listener()

        if isinstance(out, logging.FileHandler):
            if self.verbose and out.baseFilename:
                self.message(f"Standard output will be saved into `{out.baseFilename}`")

        if isinstance(err, logging.FileHandler):
            if self.verbose and err.baseFilename:
                self.message(f"Standard error will also be saved into `{err.baseFilename}`")

        if out:
            out.setFormatter(LoggerFormatter())
            out.addFilter(self._filter_out)  # Configure output handler for INFO and DEBUG
            self.logger.addHandler(out)

        if err:
            err.setFormatter(LoggerFormatter())
            err.addFilter(self._filter_err)  # Configure error handler for WARNING and ERROR
            self.logger.addHandler(err)

        self._start_queue_listener()

    def _start_queue_listener(self):

        handlers = [handler for handler in self.logger.handlers if not isinstance(handler, QueueHandler)]
        if not any(isinstance(handler, QueueHandler) for handler in self.logger.handlers):

            queue_handler = QueueHandler(self.queue)
            self.logger.addHandler(queue_handler)

            self.queue_listener = QueueListener(self.queue, *handlers)
            self.queue_listener.start()

    def _stop_queue_listener(self):

        if self.queue_listener:
            self.queue_listener.stop()
            self.queue_listener = None

    def _filter_out(self, record):  # Filter to allow DEBUG and INFO messages
        return record.levelno <= logging.INFO

    def _filter_err(self, record):  # Filter to allow WARNING, ERROR, and CRITICAL messages
        return record.levelno >= logging.WARNING

    def debug(self, *args, **kwargs):

        # Refresh level and verbose status
        verbose = self.verbose if self.verbose is not None else Logger.verbose
        if verbose:
            self.logger.setLevel(logging.DEBUG)

        self.clearline()
        kwargs["stacklevel"] = kwargs.get("stacklevel", 0) + 2
        self.logger.debug(self.format(*args), **kwargs)

    def message(self, *args, **kwargs):
        if self.quiet or (self.quiet is None and Logger.quiet):
            return

        self.clearline()
        kwargs["stacklevel"] = kwargs.get("stacklevel", 0) + 2
        self.logger.info(self.format(*args), **kwargs)

    def info(self, *args, **kwargs):
        if self.quiet or (self.quiet is None and Logger.quiet):
            return

        self.clearline()
        kwargs["stacklevel"] = kwargs.get("stacklevel", 0) + 2
        self.logger.info(self.format(*args), **kwargs)

    def exception(self, *args, **kwargs):
        self.clearline()
        kwargs["stacklevel"] = kwargs.get("stacklevel", 0) + 2
        self.logger.exception(self.format(*args), **kwargs)

    def warning(self, *args, **kwargs):
        self.clearline()
        kwargs["stacklevel"] = kwargs.get("stacklevel", 0) + 2
        self.logger.warning(self.format(*args), **kwargs)

    def error(self, *args, **kwargs):
        self.clearline()
        kwargs["stacklevel"] = kwargs.get("stacklevel", 0) + 2
        self.logger.error(self.format(*args), **kwargs)

    def fatal(self, *args, **kwargs):
        self.clearline()
        kwargs["stacklevel"] = kwargs.get("stacklevel", 0) + 2
        self.logger.critical(self.format(*args), **kwargs)
        sys.exit(1)  # Ensure the program exits with the error message

    def format(self, *args, **kwargs):

        vars = kwargs.get("vars", ["PWD", "HOME", "TMPDIR"])
        msg = ' '.join(map(str, args))

        if isinstance(vars, list):
            for key in vars:
                if key in os.environ:
                    value = os.environ[key]
                    slash = "/" if os.environ[key].endswith("/") else ""
                    msg = msg.replace(value, f"{LoggerFormatter.purple}${key}{LoggerFormatter.reset}{slash}")

        return msg

    def clearline(self):
        for handler in self.logger.handlers:
            if isinstance(handler, StreamHandler):
                handler.clearline()

    def __del__(self):
        self.close_handlers()  # Ensure handlers are closed when the object is destroyed
